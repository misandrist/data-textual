Data-Textual
============
This package provides an interface for converting between data and its
(human-friendly) textual representation.

Installation
------------
The usual:

	$ cabal install

